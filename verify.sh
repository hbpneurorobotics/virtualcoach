#!/bin/bash
# This script is designed for local usage.

# Ignore generated files and files provided for testing.
export IGNORE_LINT="platform_venv|config_files|examples/integration_test/test_experiment_folder"
export VIRTUAL_ENV=$NRP_VIRTUAL_ENV

# This script only runs static code analysis, the tests can be run separately using run_tests.sh
make run_pycodestyle run_pylint
RET=$?

if [ $RET == 0 ]; then
    echo -e "\033[32mVerify successful.\e[0m Run ./run_tests.sh to run the tests."
else
    echo -e "\033[31mVerify failed.\e[0m"
fi

exit $RET
