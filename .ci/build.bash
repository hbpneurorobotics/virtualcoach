#!/bin/bash
set -e
set -x

# Debug printing
whoami
env | sort

if [ -z "${HBP}" ]; then
    echo "USAGE: The HBP variable not specified"
    exit 1
fi

if [ -z ${PYTHON_VERSION_MAJOR_MINOR} ]; then
    export PYTHON_VERSION_MAJOR=$(python -c "import sys; print(sys.version_info.major)")
    export PYTHON_VERSION_MAJOR_MINOR=$(python -c "import sys; print('{}.{}'.format(sys.version_info.major, sys.version_info.minor))")
fi

# Force the build to happen in the build workspace
cd $HBP
export PYTHONPATH=

VIRTUAL_COACH_DIR=${GIT_CHECKOUT_DIR:-"VirtualCoach"}

cd ${HBP}/${VIRTUAL_COACH_DIR}


# Configure build
export NRP_INSTALL_MODE=dev
export PYTHONPATH=hbp_nrp_virtual_coach:$PYTHONPATH

# Build
export IGNORE_LINT='platform_venv|config_files|examples/integration_test/test_experiment_folder'
# verify_base-ci fails on dependencies mismatch, but ignores linter errors, which are cought by Jenkins afterwards
rm -rf build_env
virtualenv build_env \
    && . build_env/bin/activate \
    && make --always-make verify_base-ci
