'''setup.py'''

# pylint: disable=F0401,E0611,W0622,E0012,W0142,W0402

from setuptools import setup
import pathlib
import pynrp

README = (pathlib.Path(__file__).parent / "README.txt").read_text()

reqs_file = './requirements.txt'
install_reqs = list(val.strip() for val in open(reqs_file))
reqs = install_reqs

config = {
    'description': 'Python interface to the Neurorobotics Platform (NRP)',
    'long_description': README,
    'long_description_content_type': 'text/markdown',
    'author': 'HBP Neurorobotics',
    'url': 'http://neurorobotics.net',
    'author_email': 'neurorobotics@humanbrainproject.eu',
    'version': pynrp.__version__,
    'install_requires': reqs,
    'packages': ['pynrp'],
    'package_data': {
        'pynrp': ['config.json']
    },
    'classifiers': [
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10'],
    'scripts': [],
    'name': 'pynrp',
    'include_package_data': True,
}

setup(**config)
