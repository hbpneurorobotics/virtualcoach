# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
Unit tests for the Virtual Coach main interface.
"""

from future import standard_library
standard_library.install_aliases()
from builtins import object
from pynrp.virtual_coach import VirtualCoach
import pynrp.version

from unittest.mock import Mock, patch, MagicMock
import unittest
import requests
import getpass
import logging

import copy
from dateutil import parser
import json
from io import StringIO
import os


class TestVirtualCoach(unittest.TestCase):
    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_storage_token')
    @patch('getpass.getpass', return_value='password')
    def setUp(self, mock_getpass, mock_storage_login):

        # cause the rospy import to fail since we can't include it as a dependency
        # (for Collab support and build support without a running roscore)
        # overriden with Mock for specific test case behavior as needed (see below)
        import builtins
        realimport = builtins.__import__

        def rospy_import_fail(name, globals=globals(), locals=locals(), fromlist=[], level=-1):
            if name == 'rospy':
                raise ImportError('no ROS for tests')
            try:
                return realimport(name, globals, locals, fromlist, level)
            except:
                pass
            return realimport(name, globals, locals, fromlist, 0)
        builtins.__import__ = rospy_import_fail

        self._tests_path = os.path.dirname(os.path.realpath(__file__))

        self._vc = VirtualCoach(environment='localhost', storage_username='nrpuser')

        self._mock_available_servers_list = [
                                                {"id": 'mock-server-1'},
                                                {"id": 'mock-server-4'},
                                                {"id": 'mock-server-5'}
                                            ]

        self._mock_exp_list = {'MockExperiment1': {'configuration': {'name': 'C',
                                                                     'maturity': 'production',
                                                                     'description': 'Mock C',
                                                                     'timeout': 840,
                                                                     'experimentConfiguration': 'foo/bar1.xml'},
                                                   'joinableServers': [{'server': 'mock-server-3',
                                                                        'runningSimulation': {'owner': '1',
                                                                                              'state': 'paused',
                                                                                              'creationDate': 'Feb 03 12:11:10 UTC 2017'}},
                                                                       {'server': 'mock-server-6',
                                                                        'runningSimulation': {'owner': '3',
                                                                                              'state': 'started',
                                                                                              'creationDate': 'Feb 03 12:15:24 UTC 2017'}}]
                                                   },
                               'MockExperiment2': {'configuration': {'name': 'A',
                                                                     'maturity': 'production',
                                                                     'description': 'Mock A',
                                                                     'timeout': 900,
                                                                     'experimentConfiguration': 'foo/bar3.xml'},
                                                   'joinableServers': []
                                                   },
                               'MockDevExperiment': {'configuration': {'name': 'B',
                                                                       'maturity': 'development',
                                                                       'description': 'Mock B',
                                                                       'timeout': 780,
                                                                       'experimentConfiguration': 'foo/bar2.xml'},
                                                     'joinableServers': [{'server': 'mock-server-2',
                                                                          'runningSimulation': {'owner': '2',
                                                                                                'state': 'created',
                                                                                                'creationDate': 'Feb 03 12:07:58 UTC 2017'}}]
                                                     }}

        self._mock_exp_list_local = {'MockExperiment1': {'configuration': {'name': 'C',
                                                                           'maturity': 'production',
                                                                           'description': 'Mock C',
                                                                           'timeout': 840,
                                                                           'experimentConfiguration': 'foo/bar1.xml'},
                                                         'joinableServers': [{'server': 'localhost',
                                                                              'runningSimulation': {'owner': '1',
                                                                                                    'state': 'paused',
                                                                                                    'creationDate': 'Feb 03 12:11:10 UTC 2017'}}]
                                                         },
                                     'MockExperiment2': {'configuration': {'name': 'A',
                                                                           'maturity': 'production',
                                                                           'description': 'Mock A',
                                                                           'timeout': 900,
                                                                           'experimentConfiguration': 'foo/bar3.xml'},
                                                         'joinableServers': []
                                                         }}

        self._mock_exp_list_cloned = [{'uuid': 'MockExperiment1_0', 'name': 'MockExperiment1_0'},
                                      {'uuid': 'MockExperiment2_0', 'name': 'MockExperiment2_0'}]


        self._mock_model_list = {'example_model_1':  {"name":"example_model_1",
                                                      "displayName":"Example Model 1",
                                                      "type":"robots",
                                                      "isShared":"false",
                                                      "isCustom":"false",
                                                      "description":"Example Model 1 Description.",
                                                      "thumbnail":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA",
                                                      "path":"example_model_1",
                                                      "sdf":"example_model_1.sdf",
                                                      "configPath":"example_model_1/model.config"},
                                 'example_model_2':  {"name":"example_model_2",
                                                      "displayName":"Example Model 2",
                                                      "type":"robots",
                                                      "isShared":"true",
                                                      "isCustom":"true",
                                                      "description":"Example Model 2 Description.",
                                                      "thumbnail":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA",
                                                      "path":"example_model_2",
                                                      "sdf":"example_model_2.sdf",
                                                      "configPath":"example_model_2/model.config"}}


    def test_init_asserts_no_password(self):
        # invalid environment
        self.assertRaises(AssertionError, VirtualCoach, environment=True)


    @patch('getpass.getpass', return_value='password')
    def test_init_asserts(self, mock_getpass):
        # invalid oidc username
        self.assertRaises(AssertionError, VirtualCoach, oidc_username=123)


        # invalid storage server username
        self.assertRaises(AssertionError, VirtualCoach, storage_username=123)

    def test_no_login_credentials(self):
        self.assertRaises(Exception, VirtualCoach)

    def test_version(self):
        self.assertRegex(pynrp.version._get_version(), "^\d+.\d+.\d+")
        self.assertRegex(pynrp.version.VERSION, "^\d+.\d+.\d+")

    @patch('os.getenv', return_value="/non-existing-path")
    def test_version_exception(self, mock_getenv):
        self.assertRaises(RuntimeError, pynrp.version._get_version)


    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_storage_token')
    @patch('getpass.getpass', return_value='password')
    def test_storage_auth(self, mock_getpass, mock_storage_login):
        # mocked storage authentication, ensure called if username provided
        VirtualCoach(environment='localhost', storage_username='nrpuser')
        mock_storage_login.assert_called_once_with('nrpuser', 'password')

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_oidc_token')
    @patch('getpass.getpass', return_value='password')
    def test_storage_auth(self, mock_getpass, mock_storage_login):
        # mocked oidc authentication, ensure called if username provided
        VirtualCoach(environment='localhost', oidc_username='nrpuser')
        mock_storage_login.assert_called_once_with('nrpuser', 'password')

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    @patch('sys.stdout', new_callable=StringIO)
    def test_print_templates(self, mock_stdout, mock_list):
        # invalid dev option (should be True or False; it is set to False by default)
        self.assertRaises(AssertionError, self._vc.print_templates, 'foo')


        # mock the server call
        mock_list.return_value = self._mock_exp_list
        self._vc.print_templates()

        prod_table = """
+-----------------+------+--------------------+-------------+
|  Configuration  | Name | Configuration Path | Description |
+=================+======+====================+=============+
| MockExperiment2 | A    | foo/bar3.xml       | Mock A      |
+-----------------+------+--------------------+-------------+
| MockExperiment1 | C    | foo/bar1.xml       | Mock C      |
+-----------------+------+--------------------+-------------+
        """
        self.assertEqual(mock_stdout.getvalue().strip(), prod_table.strip())

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    @patch('sys.stdout', new_callable=StringIO)
    def test_print_templates_dev(self, mock_stdout, mock_list):

        # mock the Storage server calls
        mock_list.return_value = self._mock_exp_list
        self._vc.print_templates(True)

        dev_table = """
+---------------------------------+------+--------------------+-------------+
|          Configuration          | Name | Configuration Path | Description |
+=================================+======+====================+=============+
| MockExperiment2 (production)    | A    | foo/bar3.xml       | Mock A      |
+---------------------------------+------+--------------------+-------------+
| MockDevExperiment (development) | B    | foo/bar2.xml       | Mock B      |
+---------------------------------+------+--------------------+-------------+
| MockExperiment1 (production)    | C    | foo/bar1.xml       | Mock C      |
+---------------------------------+------+--------------------+-------------+
        """
        self.assertEqual(mock_stdout.getvalue().strip(), dev_table.strip())

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    @patch('pynrp.virtual_coach.datetime')
    @patch('sys.stdout', new_callable=StringIO)
    def test_print_running_experiments_local(self, mock_stdout, mock_date, mock_list):

        # mock the OIDC server call
        mock_list.return_value = self._mock_exp_list_local

        # mock the current time (so that the table prints consistently)
        mock_date.now = Mock(return_value=parser.parse('Feb 03 12:20:03 UTC 2017'))

        self._vc.print_running_experiments()

        running_table = """
+-----------------+------------+--------+---------+--------+-----------+
|  Configuration  |   Owner    |  Time  | Timeout | State  |  Server   |
+=================+============+========+=========+========+===========+
| MockExperiment1 | local_user | 0:08:5 | 0:14:00 | paused | localhost |
+-----------------+------------+--------+---------+--------+-----------+
        """
        self.assertEqual(mock_stdout.getvalue().strip(), running_table.strip())

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    @patch('pynrp.virtual_coach.datetime')
    @patch('sys.stdout', new_callable=StringIO)
    def test_print_running_experiments(self, mock_stdout, mock_date, mock_list):
        self._vc._VirtualCoach__oidc_username = 'user'
        # mock the Storage/OIDC server call
        mock_list.return_value = self._mock_exp_list

        # mock the user lookup Storage/OIDC call
        def fake_user_lookup(url):
            i = url.rsplit('/')[-1]
            if i == '1':
                return None, json.dumps({'displayName': 'User One'})
            if i == '2':
                return None, json.dumps({'displayName': 'User Two'})
            if i == '3':
                return None, json.dumps({'displayName': 'User Three'})

        self._vc._VirtualCoach__http_client.get = Mock()
        self._vc._VirtualCoach__http_client.get.side_effect = fake_user_lookup

        # mock the current time (so that the table prints consistently)
        mock_date.now = Mock(return_value=parser.parse('Feb 03 12:20:03 UTC 2017'))
        self._vc.print_running_experiments()

        running_table = """
+-------------------+------------+--------+---------+---------+---------------+
|   Configuration   |   Owner    |  Time  | Timeout |  State  |    Server     |
+===================+============+========+=========+=========+===============+
| MockDevExperiment | User Two   | 0:12:0 | 0:13:00 | created | mock-server-2 |
+-------------------+------------+--------+---------+---------+---------------+
| MockExperiment1   | User One   | 0:08:5 | 0:14:00 | paused  | mock-server-3 |
+-------------------+------------+--------+---------+---------+---------------+
| MockExperiment1   | User Three | 0:04:3 | 0:14:00 | started | mock-server-6 |
+-------------------+------------+--------+---------+---------+---------------+
        """
        self.assertEqual(mock_stdout.getvalue().strip(), running_table.strip())

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_available_server_list')
    @patch('sys.stdout', new_callable=StringIO)
    def test_print_available_servers(self, mock_stdout, available_servers):

        # mock the GET server call
        available_servers.return_value = self._mock_available_servers_list
        self._vc.print_available_servers()

        available_servers = """
mock-server-1
mock-server-4
mock-server-5
        """
        self.assertEqual(mock_stdout.getvalue().strip(), available_servers.strip())

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_available_server_list')
    @patch('sys.stdout', new_callable=StringIO)
    def test_print_no_available_servers(self, mock_stdout, available_servers):

        # mock the HTTP server call
        available_servers.return_value = []
        self._vc.print_available_servers()

        available_servers = 'No available servers.'
        self.assertEqual(mock_stdout.getvalue().strip(), available_servers.strip())

    def test_get_experiment_list(self):

        # mock the request (storage)
        self._vc._VirtualCoach__http_client.get = Mock()
        self._vc._VirtualCoach__http_client.get.return_value = 'x', json.dumps(self._mock_exp_list)

        list_json = self._vc._VirtualCoach__get_experiment_list()
        self.assertEqual(list_json, self._mock_exp_list)

    @patch('requests.get')
    def test_get_cloned_experiment_list(self, mock_request):
        mock_response = requests.Response
        mock_response.content = json.dumps(self._mock_exp_list_cloned)
        mock_request.return_value = mock_response
        expected_list = {'MockExperiment2_0': {'uuid': 'MockExperiment2_0', 'name': 'MockExperiment2_0'}, 'MockExperiment1_0': {'uuid': 'MockExperiment1_0', 'name': 'MockExperiment1_0'}}
        exp_list = self._vc._VirtualCoach__get_experiment_list(cloned=True)
        self.assertEqual(exp_list, expected_list)

    def test_launch_asserts(self):
        self.assertRaises(AssertionError, self._vc.launch_experiment, None)
        self.assertRaises(AssertionError, self._vc.launch_experiment, 'foo', True)
        self.assertRaises(AssertionError, self._vc.launch_experiment, 'foo', None, False)

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    def test_launch_invalid_experiment(self, mock_list):
        # mock the Storage/OIDC server call
        mock_list.return_value = self._mock_exp_list
        self.assertRaises(ValueError, self._vc.launch_experiment, 'InvalidExperimentID')

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_available_server_list')
    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    def test_launch_invalid_server(self, mock_list, servers_list):

        # mock the experiments and servers call
        mock_list.return_value = self._mock_exp_list
        servers_list.return_value = self._mock_available_servers_list

        self.assertRaises(ValueError, self._vc.launch_experiment, 'MockExperiment1', 'invalid-server-1')

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_available_server_list')
    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    def test_launch_no_available_servers(self, mock_list, servers_list):
        # mock the Storage server call
        mock_list.return_value = self._mock_exp_list
        servers_list.return_value = []
        self.assertRaises(ValueError, self._vc.launch_experiment, 'MockExperiment1')

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_available_server_list')
    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    @patch('pynrp.simulation.Simulation.__init__')
    @patch('pynrp.simulation.Simulation.launch')
    def test_launch_one_server(self, mock_sim_launch, mock_sim, mock_list, servers_list):

        # mock sim launch to succeed
        mock_sim.return_value = None
        mock_sim_launch.return_value = True

        # mock the GET server call
        servers_list.return_value = self._mock_available_servers_list

        mock_list.return_value = self._mock_exp_list
        self._vc.launch_experiment('MockExperiment1', 'mock-server-4')

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_available_server_list')
    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    @patch('pynrp.simulation.Simulation.__init__')
    @patch('pynrp.simulation.Simulation.launch')
    def test_launch_any(self, mock_sim_launch, mock_sim, mock_list, servers_list):

        # mock sim launch to succeed
        mock_sim.return_value = None
        self._vc._VirtualCoach__http_headers = {'Authorization': 'token'}

        def launch(experiment_id, experiment_conf, server, reservation, cloned, token,
                   brain_processes=1, profiler='disabled', recordingPath=None):
            if server == 'mock-server-1':
                raise Exception('fake failure!')
            return True
        mock_sim_launch.side_effect = launch

        # mock the experiments and servers call
        mock_list.return_value = self._mock_exp_list
        servers_list.return_value = self._mock_available_servers_list
        self._vc.launch_experiment('MockExperiment1')

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    @patch('pynrp.simulation.Simulation.__init__')
    @patch('pynrp.simulation.Simulation.launch')
    def test_launch_all_fail(self, mock_sim_launch, mock_sim, mock_list):

        # mock sim launch call to throw exception / etc
        mock_sim.return_value = None
        mock_sim_launch.return_value = False

        # mock the Storage/OIDC server call
        mock_list.return_value = self._mock_exp_list
        self.assertRaises(Exception, self._vc.launch_experiment, 'MockExperiment1')

    def test_clone_experiment_to_storage_assert(self):
        self.assertRaises(AssertionError, self._vc.clone_experiment_to_storage, 123)

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    @patch('requests.post')
    def test_clone_experiment_to_storage_fail(self, request, mock_list):
        mock_list.return_value = self._mock_exp_list

        class Request(object):
            status_code = 477

        request.return_value = Request()
        self.assertRaises(Exception, self._vc.clone_experiment_to_storage, 'MockExperiment1')

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    def test_clone_invalid_experiment(self, mock_list):
        mock_list.return_value = self._mock_exp_list
        self.assertRaises(ValueError, self._vc.clone_experiment_to_storage, 'invalid_configuration')

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    @patch('pynrp.virtual_coach.logger.info')
    def test_clone_experiment_to_storage(self, mock_logger, mock_list):
        mock_list.return_value = self._mock_exp_list
        self._vc._VirtualCoach__http_client = Mock(post=Mock(return_value=[200, b'experiment_id']))
        self._vc.clone_experiment_to_storage('MockExperiment1')
        self.assertEqual(mock_logger.call_count, 1)

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_storage_token')
    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    def test_delete_cloned_experiment_failed(self, mock_list, mock_login):
        mock_list.return_value = ['MockExperiment1_0', 'MockExperiment2_0']
        self.assertRaises(ValueError, self._vc.delete_cloned_experiment,  'foo')

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    def test_delete_cloned_experiment(self, mock_list):
        self._vc._VirtualCoach__http_client = Mock(delete=Mock())
        mock_list.return_value = ['MockExperiment1_0', 'MockExperiment2_0']
        self._vc.delete_cloned_experiment('MockExperiment1_0')
        self.assertEqual(self._vc._VirtualCoach__http_client.delete.call_count, 1)

    def test_clone_cloned_experiment(self):
        self.assertRaises(AssertionError, self._vc.clone_cloned_experiment, 123)

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    def test_clone_missing_experiment(self, mock_list):
        mock_list.return_value = []
        self.assertRaises(ValueError, self._vc.clone_cloned_experiment, 'missing_id')

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    @patch('requests.post')
    def test_clone_cloned_to_storage_fail(self, request, mock_list):
        mock_list.return_value = ['exp_id']

        class Request(object):
            status_code = 477

        request.return_value = Request()
        self._vc._VirtualCoach__storage_username = None
        self.assertRaises(ValueError, self._vc.clone_cloned_experiment, 'exp_id')

    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    @patch('requests.post')
    def test_clone_cloned_to_storage(self, request, mock_list):
        mock_list.return_value = ['exp_id']

        class Request(object):
            status_code = 200
            content = None

        request.return_value = Request()
        self._vc.clone_cloned_experiment('exp_id')


    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_experiment_list')
    @patch('sys.stdout', new_callable=StringIO)
    def test_print_cloned_experiments(self, mock_stdout, mock_list):
        self._vc._VirtualCoach__storage_username = 'token'
        mock_list.return_value = ['MockExperiment1_0', 'MockExperiment2_0']
        self._vc.print_cloned_experiments()
        cloned_experiments = """
+-------------------+
|       Name        |
+===================+
| MockExperiment1_0 |
+-------------------+
| MockExperiment2_0 |
+-------------------+
        """
        self.assertEqual(mock_stdout.getvalue().strip(), cloned_experiments.strip())

    def test_get_storage_token_asserts(self):
        self.assertRaises(AssertionError, self._vc._VirtualCoach__get_storage_token, 123, 'foo')
        self.assertRaises(AssertionError, self._vc._VirtualCoach__get_storage_token, 'foo', 123)

    @patch('requests.post')
    def test_get_storage_token_fail(self, mock_request):

        class Response(object):
            status_code = 500

        mock_request.return_value = Response()
        self.assertRaises(Exception, self._vc._VirtualCoach__get_storage_token, 'user', 'pass')

    @patch('requests.post')
    def test_get_storage_token(self, mock_request):

        class Response(object):
            status_code = 200
            content = 'token'

        mock_request.return_value = Response()
        content = self._vc._VirtualCoach__get_storage_token('user', 'pass')
        self.assertEqual(content, 'token')

    def __mock_csv_files_response(self):
        class Response(object):
            status_code = 200
            content = """
            [
                { "name": "csv1", "folder": "folder1", "size":3, "uuid": "uuid1" },
                { "name": "csv2", "folder": "folder1", "size":2, "uuid": "uuid2" },
                { "name": "csv3", "folder": "folder1", "size":4, "uuid": "uuid3" },
                { "name": "csv1", "folder": "folder2", "size":5, "uuid": "uuid4" }
            ]
            """

        return Response()

    @patch('requests.get')
    @patch('sys.stdout', new_callable=StringIO)
    def test_print_experiment_runs(self, mock_stdout,mock_request):
        mock_request.return_value = self.__mock_csv_files_response()

        content = self._vc.print_experiment_runs_files('exp_id', 'csv')
        csv_runs = """
+--------+---------+-------+
| Run id |  Date   | Bytes |
+========+=========+=======+
|      0 | folder1 |     9 |
+--------+---------+-------+
|      1 | folder2 |     5 |
+--------+---------+-------+
        """
        self.assertEqual(mock_stdout.getvalue().strip(), csv_runs.strip())

    @patch('requests.get')
    @patch('sys.stdout', new_callable=StringIO)
    def test_print_experiment_run_files(self, mock_stdout,mock_request):
        mock_request.return_value  = self.__mock_csv_files_response()

        self._vc.print_experiment_run_files('exp_id', 'csv', 0)
        csv_run_files = """
+------+------+
| File | Size |
+======+======+
| csv1 |    3 |
+------+------+
| csv2 |    2 |
+------+------+
| csv3 |    4 |
+------+------+
"""
        self.assertEqual(mock_stdout.getvalue().strip(), csv_run_files.strip())

    @patch('requests.get')
    @patch('sys.stdout', new_callable=StringIO)
    def test_print_last_run_files_csv(self, mock_stdout,mock_request):
        mock_request.return_value  = self.__mock_csv_files_response()

        self._vc.print_last_run_files('exp_id', 'csv')
        csv_run_files = """
+------+------+
| File | Size |
+======+======+
| csv1 |    5 |
+------+------+
"""
        self.assertEqual(mock_stdout.getvalue().strip(), csv_run_files.strip())

    def __mock_csv_file_response(self):
        class Response(object):
            status_code = 200
            content ='a,b,c\n1,2,3'

        return Response()

    @patch('requests.get')
    def test_get_experiment_run_file_csv(self, mock_request):
        mock_request.side_effect = [
            self.__mock_csv_files_response(),
            self.__mock_csv_file_response()
        ]
        self._vc._VirtualCoach__get_file_content = MagicMock(side_effect=self._vc._VirtualCoach__get_file_content)
        csv_content = self._vc.get_experiment_run_file('exp_id', 'csv', 0, 'csv1')

        self.assertEqual(csv_content, 'a,b,c\n1,2,3')
        self._vc._VirtualCoach__get_file_content.assert_called_once_with('exp_id', u'uuid1')

    @patch('requests.get')
    def test_get_last_run_file_csv(self, mock_request):
        mock_request.side_effect = [
            self.__mock_csv_files_response(),
            self.__mock_csv_file_response()
        ]

        self._vc._VirtualCoach__get_file_content = MagicMock(side_effect=self._vc._VirtualCoach__get_file_content)
        csv_content = self._vc.get_last_run_file('exp_id', 'csv', 'csv1')
        self.assertEqual(csv_content, 'a,b,c\n1,2,3')
        self._vc._VirtualCoach__get_file_content.assert_called_once_with('exp_id', u'uuid4')

    @patch('requests.post')
    def test_set_experiment_list(self, request):
        class Request(object):
            status_code = 200
            content = None

        request.return_value = Request()
        self._vc.set_experiment_file('exp_id', 'file_name', 'file_content')

    @patch('requests.post')
    def test_import_experiment_zip(self, mock_request):
        mock_request.side_effect = [MagicMock(status_code=requests.codes.ok)]
        self.assertRaises(Exception, self._vc.import_experiment, 'imaginary_file.zip')
        path = os.path.join(self._tests_path, 'test.zip')
        response = self._vc.import_experiment(path)
        self.assertEqual(response.status_code, requests.codes.ok)

    @patch('requests.post')
    def test_import_experiment_folder(self, mock_request):
        mock_request.side_effect = [MagicMock(status_code=requests.codes.ok)]
        self.assertRaises(Exception, self._vc.import_experiment, 'imaginary_experiment_folder')
        path = os.path.join(self._tests_path, 'test_experiment_folder')
        response = self._vc.import_experiment(path)
        self.assertEqual(response.status_code, requests.codes.ok)


    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_model_list')
    @patch('sys.stdout', new_callable=StringIO)
    def test_print_available_models(self, mock_stdout, mock_list):
        # invalid dev option (should be True or False; it is set to False by default)
        self.assertRaises(AssertionError, self._vc.print_available_models, 23)

        # mock the server call
        mock_list.return_value = self._mock_model_list
        self._vc.print_available_models(model_type='robots', user='all')

        model_table = """
+--------------+--------------+--------------+--------------+--------------+
|     Name     | Display Name |   isShared   |   isCustom   | Description  |
+==============+==============+==============+==============+==============+
| example_mode | Example      | false        | false        | Example      |
| l_1          | Model 1      |              |              | Model 1      |
|              |              |              |              | Description. |
+--------------+--------------+--------------+--------------+--------------+
| example_mode | Example      | true         | true         | Example      |
| l_2          | Model 2      |              |              | Model 2      |
|              |              |              |              | Description. |
+--------------+--------------+--------------+--------------+--------------+
        """
        self.assertEqual(mock_stdout.getvalue().strip(), model_table.strip())

    @patch('requests.post')
    def test_import_model(self, mock_request):
        mock_request.side_effect = [MagicMock(status_code=requests.codes.ok)]
        self.assertRaises(Exception, self._vc.import_model, 'imaginary_file.zip')
        path = os.path.join(self._tests_path, 'test.zip')
        response = self._vc.import_model(path=path, model_type='robots')
        self.assertEqual(response.status_code, requests.codes.ok)


    @patch('pynrp.virtual_coach.VirtualCoach._VirtualCoach__get_model_list')
    def test_delete_model(self, mock_list):
        mock_list.return_value = self._mock_model_list
        self.assertRaises(ValueError, self._vc.delete_model,  'foo_name', 'foo_type')
