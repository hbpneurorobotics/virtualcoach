# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
Unit tests for the Virtual Coach simulation interface.
"""

from future import standard_library
standard_library.install_aliases()
from pynrp.simulation import Simulation

from pynrp.requests_client import RequestsClient
from pynrp.config import Config

from unittest.mock import Mock, patch, call, MagicMock
import unittest

import http.client
from urllib.error import HTTPError
import json
from io import StringIO


class TestSimulation(unittest.TestCase):

    def setUp(self):
        self._sim = Simulation(RequestsClient({'Authorization': 'token'}), Config('localhost'),
                               None)

    def setUpForLaunch(self):
        get_response = (None,
                        '{"serverJobLocation": "mock-location", '
                        '"gzweb": {"nrp-services": "mock-services"}, '
                        '"rosbridge": {"websocket": "mock-socket"}}')
        post_response = (http.client.CREATED, '{"simulationID": "12"}')
        self._sim._Simulation__http_client.get = Mock(return_value=get_response)
        self._sim._Simulation__http_client.post = Mock(return_value=post_response)

    def test_init_asserts(self):
        self.assertRaises(AssertionError, Simulation, None, Config('localhost'), None)
        self.assertRaises(AssertionError, Simulation, RequestsClient({'Authorization': 'token'}),
                          None, None)

    def test_launch_asserts(self):
        self.assertRaises(AssertionError, self._sim.launch, None, 'conf', 'server', None)
        self.assertRaises(AssertionError, self._sim.launch, 'id', None, 'server', None)
        self.assertRaises(AssertionError, self._sim.launch, 'id', 'conf', None, None)
        self.assertRaises(AssertionError, self._sim.launch, 'id', 'conf', 'server', True)

    @patch('traceback.print_exc')
    def test_failed_server_info(self, mocked_traceback):
        # mock HTTP call to throw Exception
        self.setUpForLaunch()
        self._sim._Simulation__http_client.get = Mock()
        self._sim._Simulation__http_client.get.side_effect = Exception('something bad failed')

        # make sure it returns failure
        self.assertFalse(self._sim.launch('id', 'conf', 'server', None))
        self._sim._Simulation__http_client.get.assert_called_once()
        self._sim._Simulation__http_client.post.assert_not_called()
        mocked_traceback.assert_called_once()

    @patch('traceback.print_exc')
    def test_failed_create_conflict(self, mocked_traceback):
        self.setUpForLaunch()
        self._sim._Simulation__http_client.post = Mock(return_value=(http.client.CONFLICT, '{}'))

        self.assertFalse(self._sim.launch('id', 'conf', 'server-name', None))
        mocked_traceback.assert_called_once()

        self._sim._Simulation__http_client.post.assert_called_once()

    @patch('traceback.print_exc')
    def test_failed_create_other(self, mocked_traceback):
        self.setUpForLaunch()
        self._sim._Simulation__http_client.post = Mock(return_value=(http.client.NOT_FOUND, '{}'))

        self.assertEqual(self._sim.launch('id', 'conf', 'server-name', None), False)

        self._sim._Simulation__http_client.post.assert_called_once()
        mocked_traceback.assert_called_once()

    @patch('pynrp.rosbridge_handler.RosBridgeHandlerProcess.initialize')
    def test_create(self, _mocked_init):
        self.setUpForLaunch()

        # mock the call to set simulation state
        self._sim._Simulation__set_state = Mock()

        self.assertEqual(self._sim.launch('id', 'conf', 'server-name', 'reservation'), True)

        # calling launch twice on an instance should fail after successful creation
        self.assertRaises(Exception, self._sim.launch, 'id', 'conf', 'server-name', 'reservation')

    @patch('pynrp.rosbridge_handler.RosBridgeHandlerProcess.initialize')
    def test_create_cloned(self, _mocked_init):
        self.setUpForLaunch()
        # mock the call to set simulation state
        self._sim._Simulation__set_state = Mock()

        self.assertEqual(self._sim.launch('id', 'conf', 'server-name', 'reservation'), True)
        self.assertEqual(self._sim._Simulation__http_client.post.call_count, 1)

    def test_set_state_asserts(self):
        self.assertRaises(AssertionError, self._sim._Simulation__set_state, None)
        self.assertRaises(Exception, self._sim._Simulation__set_state, 'foo')

    def test_set_state(self):
        self._sim._Simulation__server = 'server'
        self._sim._Simulation__sim_url = 'url'

        # mock the HTTP call
        self._sim._Simulation__http_client.put = Mock(return_value=(http.client.OK, None))

        self._sim._Simulation__set_state('initialized')
        self._sim._Simulation__http_client.put.assert_called_once()

    def test_set_state_failed(self):
        self._sim._Simulation__server = 'server'
        self._sim._Simulation__sim_url = 'url'

        # mock the HTTP call
        self._sim._Simulation__http_client.put = Mock(return_value=(http.client.NOT_FOUND, None))

        self.assertRaises(Exception, self._sim._Simulation__set_state, 'started')
        self._sim._Simulation__http_client.put.assert_called_once()

    def test_states(self):
        self._sim._Simulation__server = 'server'
        self._sim._Simulation__sim_url = 'url'

        # mock the OIDC call
        self._sim._Simulation__set_state = Mock()

        # mock the ros_client
        self._sim._Simulation__ros_client = Mock()

        # start
        self._sim.start()
        self._sim._Simulation__set_state.assert_called_with('started')

        # pause
        self._sim.pause()
        self._sim._Simulation__set_state.assert_called_with('paused')

        # stop
        self._sim.stop()
        self._sim._Simulation__set_state.assert_called_with('stopped')

    def test_get_state(self):
        self._sim._Simulation__server = 'server'
        self._sim._Simulation__sim_url = 'url'

        # mock the OIDC call
        self._sim._Simulation__http_client.get = Mock(return_value=(http.client.OK,
                                                                    '{"state": "{}"}'))

        self._sim.get_state()
        assert self._sim._Simulation__http_client.get.mock_calls == [call(u'url/state')]

    def test_get_state_failed(self):
        self.assertRaises(Exception, self._sim.get_state)

        self._sim._Simulation__server = 'server'
        self._sim._Simulation__sim_url = 'url'

        # mock the OIDC call
        self._sim._Simulation__http_client.get = Mock(return_value=(http.client.NOT_FOUND,None))
        self.assertRaises(Exception, self._sim.get_state)
        self._sim._Simulation__http_client.get.assert_called_once()

    def test_get_simulation_scripts_asserts(self):
        self.assertRaises(AssertionError, self._sim._Simulation__get_simulation_scripts, None)
        self.assertRaises(AssertionError, self._sim._Simulation__get_simulation_scripts, 1)

    def test_get_simulation_scripts_failed(self):
        self.assertRaises(Exception, self._sim._Simulation__get_simulation_scripts, 'state-machine')

        self._sim._Simulation__server = 'server'
        self._sim._Simulation__sim_url = 'url'
        self._sim._Simulation__config = {'simulation-scripts': {'foo': ' '}}

        self.assertRaises(ValueError, self._sim._Simulation__get_simulation_scripts, 'bar')

        # mock the oidc call, the get_all_transfer_function call, the start call, the pause call
        # and the get_state call
        self._sim._Simulation__http_client.get = Mock(return_value=(http.client.NOT_FOUND,
                                                             None))

        self.assertRaises(Exception, self._sim._Simulation__get_simulation_scripts, 'foo')

    def test_set_script(self):
        self._sim._Simulation__server = 'server'
        self._sim._Simulation__sim_url = 'url'

        self._sim._Simulation__get_simulation_scripts = Mock()

        self.assertRaises(Exception, self._sim.add_transfer_function, 'foo')
        self._sim._Simulation__get_simulation_scripts.assert_called_once()

        self._sim._Simulation__get_simulation_scripts.return_value = {'data': {'foo': 'foo_script_name'}}

        self._sim._Simulation__http_client.put = Mock(return_value=(HTTPError, None))

        self.assertRaises(Exception, self._sim._Simulation__set_script, 'foo', 'foo_script_name',
                          'foo_script')

        self._sim.get_state = Mock()
        self._sim.get_state.return_value = 'started'

        self._sim.start = Mock()
        self._sim.pause = Mock()

        self.assertRaises(Exception, self._sim.add_transfer_function, 'foo')

        http_error = HTTPError("url", 404, "message", {}, None)

        self._sim._Simulation__http_client.put.side_effect = [http_error,
                                                            (http.client.NOT_FOUND,
                                                             None)]

        self.assertRaises(Exception, self._sim.edit_state_machine, 'foo', 'import os\n')

        self._sim._Simulation__http_client.put.side_effect = [http_error,
                                                            (http.client.OK, None)]
        self.assertRaises(Exception, self._sim.add_state_machine, 'bar', 'import os\n')

    def test_get_transfer_function_asserts(self):
        self.assertRaises(AssertionError, self._sim.get_transfer_function, None)
        self.assertRaises(Exception, self._sim.get_transfer_function, 'foo')

    def test_get_scripts(self):
        self._sim._Simulation__server = 'server'
        self._sim._Simulation__sim_url = 'url'

        self._sim._Simulation__get_simulation_scripts = Mock()
        self._sim._Simulation__get_simulation_scripts.return_value = {'data': {'foo': 'foo_code'}}

        self.assertEqual(self._sim.get_transfer_function('foo'), 'foo_code')
        self.assertEqual(self._sim.get_state_machine('foo'), 'foo_code')

    def test_get_transfer_function_failed(self):
        self._sim._Simulation__server = 'server'
        self._sim._Simulation__sim_url = 'url'

        self._sim._Simulation__get_simulation_scripts = Mock()
        self._sim._Simulation__get_simulation_scripts.return_value = {'data': {'foo': ''}}

        self.assertRaises(ValueError, self._sim.get_transfer_function, 'bar')
        self._sim._Simulation__get_simulation_scripts.assert_called_once()

    def test_set_transfer_function_asserts(self):
        self.assertRaises(AssertionError, self._sim.edit_transfer_function, None, u'foo')
        self.assertRaises(AssertionError, self._sim.edit_transfer_function, 'foo', None)
        self.assertRaises(Exception, self._sim.edit_transfer_function, 'foo', u'foo')

    def test_set_brain_asserts(self):
        self.assertRaises(AssertionError, self._sim._Simulation__set_brain, 1, {'foo': 'bar'} )

    def test_edit_brain(self):
        # mock the http call, the get_brain call, the get_populations call, the start call,
        # the pause call, the __get_simulation_scripts call and the get_state call
        self._sim.get_brain = Mock()
        self._sim.get_brain.return_value = 'brain_script'
        self._sim.get_populations = Mock()
        self._sim.get_populations.return_value = {'foo': 'bar'}
        self._sim._Simulation__get_simulation_scripts = Mock()
        self._sim._Simulation__get_simulation_scripts.return_value = {'brain_type': 'py',
                                                                'data_type': 'text'}
        self._sim._Simulation__http_client.put = Mock()
        self._sim._Simulation__http_client.put.return_value = (http.client.OK,
                                                             '{"data": {"foo": ""}}')
        self._sim.get_state = Mock()
        self._sim.get_state.return_value = 'started'
        self._sim.pause = Mock()
        self._sim.start = Mock()

        self.assertRaises(Exception, self._sim.edit_brain, '')

        self._sim._Simulation__server = 'server'
        self._sim._Simulation__sim_url = 'url'

        self._sim.edit_brain('foo')
        self._sim.start.assert_called_once()
        self._sim.pause.assert_called_once()

        # Mock http call to return an error
        self._sim._Simulation__http_client.put.return_value = (HTTPError, None)
        self.assertRaises(Exception, self._sim.edit_brain, 'import os\n')

        http_error = HTTPError("url", 404, "message", {}, None)

        self._sim._Simulation__http_client.put.side_effect = [http_error,
                                                            (http.client.NOT_FOUND,
                                                             None)]
        self.assertRaises(Exception, self._sim.edit_brain, 'foo')

        self._sim._Simulation__http_client.put.side_effect = [http_error,
                                                            (http.client.OK, None)]
        self.assertRaises(Exception, self._sim.edit_brain, 'foo')

    def test_edit_populations(self):
        self._sim._Simulation__server = 'server'
        self._sim._Simulation__sim_url = 'http://url/populations'
        self._sim.get_brain = Mock(return_value='brain_script')
        self._sim.get_populations = Mock(return_value={
            'data': {'foo': "foo_script_name"}})
        self._sim.get_state = Mock(return_value='started')
        self._sim.pause = Mock()
        self._sim.start = Mock()

        self._sim._Simulation__get_simulation_scripts = Mock(return_value={'data_type': 'foo',
                                                                           'brain_type': 'foo',
                                                                           'brain_populations': 'bar'})
        self._sim._Simulation__http_client.put = Mock(return_value=(http.client.OK,
                                                                    {}))

        self._sim.edit_populations({'foo': 'bar'})

        self._sim.start.assert_called_once()
        self._sim.pause.assert_called_once()

    def test_get_brain_and_populations(self):
        self._sim._Simulation__get_simulation_scripts = Mock()
        self._sim._Simulation__get_simulation_scripts.return_value = {'data': 'foo',
                                                                      'brain_populations': 'bar'}
        self._sim.get_populations()
        self._sim._Simulation__get_simulation_scripts.assert_called_once()
        self._sim._Simulation__get_simulation_scripts.assert_called_with('brain')

        self._sim.get_brain()
        self.assertEqual(self._sim._Simulation__get_simulation_scripts.call_count, 2)  # called twice
        self._sim._Simulation__get_simulation_scripts.assert_called_with('brain')

    def test_edit_scripts(self):
        self._sim._Simulation__server = 'server'
        self._sim._Simulation__sim_url = 'url'

        # mock the http call, the get_all_transfer_function call, the start call, the pause call
        # and the get_state call
        self._sim._Simulation__http_client.get = Mock(return_value=(http.client.OK, '{"data": {"foo": ""}}'))
        self._sim._Simulation__http_client.put = Mock(return_value=(http.client.OK, None))

        self._sim._Simulation__get_all_transfer_functions = Mock()
        self._sim._Simulation__get_all_transfer_functions.return_value = {'foo': ''}
        self._sim._Simulation__get_all_state_machines = Mock()
        self._sim._Simulation__get_all_state_machines.return_value = {'foo': ''}

        self._sim.start = Mock()
        self._sim.pause = Mock()

        self._sim.get_state = Mock()
        self._sim.get_state.return_value = 'started'

        with patch.object(self._sim, 'start') as start_mock:
            with patch.object(self._sim, 'pause') as pause_mock:
                self._sim.edit_transfer_function('foo', u'bar')
                start_mock.assert_called_once()
                pause_mock.assert_called_once()

        with patch.object(self._sim, 'start') as start_mock:
            with patch.object(self._sim, 'pause') as pause_mock:
                self._sim.edit_state_machine('foo', u'bar')
                start_mock.assert_called_once()
                pause_mock.assert_called_once()

    def test_delete_scripts(self):
        self.assertRaises(Exception, self._sim.delete_state_machine, 'foo')

        self._sim._Simulation__server = 'server'
        self._sim._Simulation__sim_url = 'url'

        # mock the http call, the get_all_transfer_function call, the start call, the pause call
        # and the get_state call
        self._sim._Simulation__http_client.delete = Mock(return_value=http.client.OK)

        self._sim._Simulation__get_simulation_scripts = Mock()
        self._sim._Simulation__get_simulation_scripts.return_value = {'data': {'foo': '', 'bar': ''}}

        self._sim.delete_state_machine('foo')
        self._sim._Simulation__http_client.delete.assert_called_once()

        self._sim.delete_transfer_function('foo')
        self.assertEqual(self._sim._Simulation__http_client.delete.call_count, 2)

        self.assertRaises(ValueError, self._sim.delete_state_machine, 'nonExistentScript')

        self._sim._Simulation__http_client.delete.return_value = (http.client.NOT_FOUND, None)
        self.assertRaises(Exception, self._sim.delete_state_machine, 'foo')


    def test_save_experiment_data(self):
        exp_id = 'exp_id'
        serverurl = 'serverurl'
        proxyurl = 'proxyurl'

        self._sim._Simulation__get_simulation_scripts = Mock()
        self._sim._Simulation__get_simulation_scripts.return_value = {'data': {'foo': ''}}
        self._sim._Simulation__sim_url = 'sim_url'
        self._sim.get_transfer_function = Mock()
        self._sim.get_transfer_function.return_value = 'bar'

        self._sim.get_state_machine = Mock()
        self._sim.get_state_machine.return_value = 'bar'

        self._sim._Simulation__experiment_id = exp_id

        self._sim._Simulation__config['proxy-services']['save-data'] = proxyurl
        self._sim._Simulation__server_info = {
            'gzweb':{
                'nrp-services': serverurl
            }
        }

        self._sim._Simulation__headers = {}

        self._sim._Simulation__http_client.put = Mock(return_value=(http.client.OK, None))

        self._sim.save_transfer_functions()
        self._sim._Simulation__http_client.put.assert_called_once_with(
            '%s/%s/transferFunctions' % (proxyurl, exp_id),
            body={'transferFunctions': ['bar'], 'experiment': exp_id})
        self._sim._Simulation__http_client.put.reset_mock()

        self._sim.save_state_machines()
        self._sim._Simulation__http_client.put.assert_called_once_with(
            '%s/%s/stateMachines' % (proxyurl, exp_id),
            body={'stateMachines': {'foo': 'bar'}, 'experiment': exp_id})
        self._sim._Simulation__http_client.put.reset_mock()

        populations = {
            'pop1': {'from': 0, 'to': 1, 'step': 1, 'regex': "^\b(?!\bpop0\b)([A-z_]+[\w_]*)$"}}

        self._sim.get_brain = Mock()
        self._sim.get_brain.return_value = 'some brain code'

        self._sim.get_populations = Mock()
        self._sim.get_populations.return_value = populations
        self._sim.save_transfer_functions = Mock()

        self._sim.save_brain()
        self._sim._Simulation__http_client.put.assert_called_once_with(
            '%s/%s/brain' % (proxyurl, exp_id),
            body={'populations': populations,'brain': 'some brain code'})

        self._sim._Simulation__http_client.post = Mock(return_value=(http.client.OK, None))
        self._sim.save_world()
        self._sim._Simulation__http_client.post.assert_called_once_with(
            '%s/sdf_world' % self._sim._Simulation__sim_url,
            body={})

    @patch('sys.stdout', new_callable=StringIO)
    def test_print_scripts(self, mock_stdout):
        self._sim._Simulation__server = 'server'
        self._sim._Simulation__sim_url = 'url'

        self._sim._Simulation__get_simulation_scripts = Mock()
        self._sim._Simulation__get_simulation_scripts.return_value = {'data': {'foo': 'one',
                                                                               'bar': 'two',
                                                                               'foobar': 'three'}}

        self._sim.print_transfer_functions()
        self._sim.print_state_machines()
        # The order in which 'foobar', 'foo' and 'bar' appear is no longer guaranteed in python 3.3+
        print_str = mock_stdout.getvalue().strip()
        self.assertEqual(len(print_str), 29)
        n = print_str.count('\n')
        self.assertEqual(n, 5)
        n = print_str.count('foobar')
        self.assertEqual(n, 2)
        n = print_str.count('foo')
        self.assertEqual(n, 4)
        n = print_str.count('bar')
        self.assertEqual(n, 4)
        #self.assertEqual(mock_stdout.getvalue().strip(), 'foobar\nfoo\nbar\nfoobar\nfoo\nbar')
        self.assertEqual(self._sim._Simulation__get_simulation_scripts.call_count, 2)

    def test_register_status_callback(self):
        # override the logger so we can check for messages
        self._sim._Simulation__logger = Mock()
        mock_callback = Mock()

        # uninitialized simulation should throw an error
        self.assertRaises(Exception, self._sim.register_status_callback, mock_callback)
        self.assertEqual(self._sim._Simulation__logger.info.call_count, 0)
        self.assertEqual(self._sim._Simulation__logger.warning.call_count, 0)

        # initialized simulation
        self._sim._Simulation__server = 'server'
        self._sim._Simulation__sim_url = 'url'

        # successful registration
        self._sim.register_status_callback(mock_callback)
        self.assertEqual(self._sim._Simulation__status_callbacks, [mock_callback])
        self._sim._Simulation__logger.info.assert_called_once_with('Status callback registered.')
        self.assertEqual(self._sim._Simulation__logger.warning.call_count, 0)

        # ignored duplicate registration
        self._sim.register_status_callback(mock_callback)
        self.assertEqual(self._sim._Simulation__status_callbacks, [mock_callback])
        self._sim._Simulation__logger.warning.assert_called_once_with('Attempting to register duplicate '
                                                                      'status callback, ignoring.')
        self.assertEqual(self._sim._Simulation__logger.info.call_count, 1)

    def test_on_error(self):
        self._sim._Simulation__logger = Mock()

        mock_msg = { 'errorType': "test", 'sourceType': "test script",
                     'message': "test body message"}

        # progress message that should be logged
        error_msg = "There was a test error resulting from the test script." \
                    " The full error is below:\n test body message"
        self._sim._Simulation__on_error(mock_msg)
        self._sim._Simulation__logger.error.assert_called_once_with(error_msg)

    def test_on_status(self):
        # override the logger so we can check for messages
        self._sim._Simulation__ros_client = Mock()
        self._sim._Simulation__logger = Mock()
        self._sim._Simulation__previous_subtask = 'foo'

        # callback to ensure it is only called when appropriate
        mock_callback = Mock()
        self._sim._Simulation__status_callbacks = [mock_callback]

        def mock_msg(msg):
            return {'data': json.dumps(msg)}

        # progress message that should be discarded
        self._sim._Simulation__on_status(mock_msg({'action': ''}))
        self._sim._Simulation__on_status(mock_msg({'progress': {'done': True}}))
        self._sim._Simulation__on_status(mock_msg({'progress': {'subtask': 'foo'}}))
        self.assertEqual(self._sim._Simulation__logger.info.call_count, 0)
        self.assertEqual(mock_callback.call_count, 0)

        # progress message that should be logged
        self._sim._Simulation__on_status(mock_msg({'progress': {'task': 'foo', 'subtask': 'bar'}}))
        self._sim._Simulation__logger.info.assert_called_once_with('[%s] %s', 'foo', 'bar')
        self.assertEqual(mock_callback.call_count, 0)

        # simulation status, callback should be invoked
        self._sim._Simulation__on_status(mock_msg({'state': 'paused'}))
        mock_callback.assert_called_once_with({'state': 'paused'})
        self.assertEqual(self._sim._Simulation__logger.info.call_count, 1) # from above

        # simulation status for end of the simulation
        self._sim._Simulation__on_status(mock_msg({'state': 'stopped'}))
        mock_callback.assert_called_with({'state': 'stopped'})
        self.assertEqual(self._sim._Simulation__ros_client.close.call_count, 1)
        self._sim._Simulation__on_status({"data": '{"state": "halted"}'})
        self.assertEqual(self._sim._Simulation__ros_client.close.call_count, 2)
        self.assertEqual(self._sim._Simulation__status_callbacks, [])
        self._sim._Simulation__logger.info.assert_called_with('Simulation has been stopped.')

    def test_reset(self):
        self._sim._Simulation__server = 'server'
        self._sim._Simulation__sim_url = 'url'

        self._sim.pause = Mock()

        self._sim._Simulation__http_client.put = Mock()
        self._sim._Simulation__http_client.put.return_value = (http.client.OK, None)

        # NRRPLT-7855
        self.assertRaises(ValueError, self._sim.reset, 'world')
        # self._sim.reset('world')
        # self._sim._Simulation__http_client.put.assert_called_once()

        self._sim._Simulation__http_client.put.return_value = (http.client.NOT_FOUND,
                                                             None)
        self._sim.start = Mock()

        self.assertRaises(ValueError, self._sim.reset, 'foo')
        # NRRPLT-7855
        self.assertRaises(ValueError, self._sim.reset, 'full')
        # self.assertRaises(Exception, self._sim.reset, 'full')
        # self._sim.start.assert_called_once()

    def test_get_csv_last_run_file(self):
        self._sim._Simulation__vc = Mock()
        self._sim._Simulation__vc.get_last_run_file = Mock()
        self._sim._Simulation__experiment_id = 'experiment_id'
        self._sim.get_csv_last_run_file('file_name')
        self._sim._Simulation__vc.get_last_run_file.assert_called_once_with('experiment_id', 'csv', 'file_name')

    def test_recording(self):
        self._sim._Simulation__config = {'simulation-services': {'recorder':'recorder'}}
        self._sim._Simulation__vc = Mock()
        self._sim._Simulation__vc.set_experiment_file = Mock()
        self._sim._Simulation__server = 'server'
        self._sim._Simulation__sim_url = 'url'
        self._sim._Simulation__vc = Mock()
        self._sim._Simulation__http_client.post = Mock()
        self._sim._Simulation__http_client.post.return_value = (http.client.OK, '{"filename":"name.txt"}')
        self._sim.start_recording()
        self._sim._Simulation__http_client.post.assert_called_with(u'url/recorder/start', body='start')
        self._sim.pause_recording()
        self._sim._Simulation__http_client.post.assert_called_with(u'url/recorder/stop', body='stop')
        self._sim.unpause_recording()
        self._sim._Simulation__http_client.post.assert_called_with(u'url/recorder/start', body='start')
        self._sim.stop_recording(True,'My description')
        self._sim._Simulation__http_client.post.assert_called_with(u'url/recorder/reset', body='reset')

if __name__ == '__main__':
    unittest.main()
