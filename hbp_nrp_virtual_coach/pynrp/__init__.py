"""
This package contains the Virtual Coach implementation and interfaces to the Neurorobotics
Platform backend services.
"""

from pynrp.version import VERSION as __version__  # pylint: disable=W0611
