.. _virtual-coach-tutorials:

..  sectionauthor:: Eloy Retamino <retamino@ugr.es>

.. seealso::

    :ref:`User manual<virtual_coach_intro>` / 
    :ref:`Code API reference<virtual-coach-api>` / 
    :ref:`Developer pages<virtual_coach_dev_space>`

Virtual Coach Tutorials
=======================

.. toctree::
    :maxdepth: 2

    tutorials/launching_exp
    tutorials/interacting_exp
