.. _hbp_nrp_virtual_coach_docs:

Virtual Coach
=============

Welcome to the Virtual Coach documentation!

.. include:: introduction.rst

----------------------------------

In the :doc:`Tutorials section<tutorials>` you can find the information on how to get started with the Virtual Coach. 

.. rubric:: Contents:

.. toctree::
   :maxdepth: 2
   
   tutorials
   developer_manual

