This package provides a command-line interface for the Neurorobotics Platform that
can be used in place of the graphical frontend interface. It is intended to support
running experiments that require many simulation iterations with modifications to
the experiment configuration in between trials.
