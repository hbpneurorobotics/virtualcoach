# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
Integration test script that can be used to verify high level Virtual Coach functionality
and interfaces with the backend.

Tests SHOULD NOT contain low level details that are subject to change in the rest of the
platform (e.g. specific text values, timings, etc.), but SHOULD interact with all high level
interfaces/APIs that are supported.
"""

# pylint: disable=W0622

from __future__ import print_function

from builtins import str
from builtins import zip
from builtins import object
import argparse
import logging
import json
import sys
import time
import traceback
import os

# virtualcoach and platform_venv specific packages, ensure environment is correct
try:
    from pynrp.virtual_coach import VirtualCoach
    import progressbar

except ImportError:
    print()
    print('Failed to import the VirtualCoach or access packages in the platform_venv! Aborting.')
    print()
    print('Please make sure you have:')
    print()
    print('\t1. run "make devinstall" in the VirtualCoach repo')
    print('\t2. use "cle-virtual-coach it.py" to run this script')
    print()
    sys.exit(-1)

#
# Test Case helper functions for results storage and progress updates.
#


# helper class for storing test case details
# pylint: disable=missing-docstring
class TestCase(object):

    def __init__(self, description):
        self.description = description
        self.time = 0.0
        self.success = False
        self.__start = time.time()

    def done(self, success):
        self.time = time.time() - self.__start
        self.success = success


# helper list container for test cases, handles progress bar updates
class TestCases(list):

    def __init__(self, num_tests):
        super(TestCases, self).__init__()
        self.__pbar = progressbar.ProgressBar(widgets=[progressbar.SimpleProgress(),
                                                       progressbar.Bar(),
                                                       progressbar.Percentage()],
                                              max_value=num_tests,
                                              term_width=80)
        self.__pbar.start()

    def start(self, description):
        self.append(TestCase('%3i. %s' % (len(self) + 1, description)))

    def done(self, success):
        self[-1].done(success)
        try:
            self.__pbar.update(self.__pbar.value + 1)
        except ValueError: # if the progressbar max_value is incorrect
            pass

    def abort(self):
        self[-1].done(False)
        self.__pbar.finish()


# helper class for failed test case
class TestCaseError(Exception):
    pass


#
# Sequential test cases, we cannot reuse the unit test framework because we must guarantee
# execution order and state at each step and abort with appropriate cleanup.
#
# pylint: disable=too-many-branches, too-many-statements, too-many-locals, redefined-outer-name
# pylint: disable=broad-except, bare-except, no-member, protected-access
def run(oidc_username, storage_username):

    # running array of test case results, unfortunately we have to hardcode the number of
    # test cases because the indeterminate progress bar is not helpful for the tester
    NUM_TEST_CASES = 28
    results = TestCases(NUM_TEST_CASES)

    try:

        # the simulation we will launch, defined here so we have a cleanup reference at any point
        sim = None
        path = os.path.dirname(os.path.abspath(__file__))
        #
        # Server Information and Experiment List Interaction
        #

        # this test validates the config.json from user-scripts and checks to make sure the
        # backend is running properly (roscore/OIDC if required)
        results.start('Init (OIDC %sabled) and config Check' % ('en' if oidc_username else 'dis'))
        vc = VirtualCoach(oidc_username=oidc_username,
                          storage_username=storage_username)
        results.done(True)

        # ability to retrieve server info (experiment/availability/etc.)
        results.start('Retrieving Server/Experiment Information')
        server_info = vc._VirtualCoach__get_experiment_list()
        results.done(True)

        # ensure there is a running server that is not currently running an experiment
        results.start('Checking For Available Backend')
        available_servers = vc._VirtualCoach__get_available_server_list()
        if not available_servers:
            raise TestCaseError('No available backends to run test on.')
        results.done(True)

        # ensure the desired IT experiment is cloned and is available in the storage server. Clone
        # the experiment first if not.
        results.start('Cloning a new Empty Template Husky Experiment from the Storage Server')

        if 'ExDTemplateHusky' not in [s[0] for s in server_info.items()]:
            raise TestCaseError('Husky Template Experiment is not available on the server to be'
                                ' cloned.')
        experiment_id = vc.clone_experiment_to_storage('ExDTemplateHusky')
        stored_experiments = vc._VirtualCoach__get_experiment_list(cloned=True)
        if experiment_id not in stored_experiments:
            raise TestCaseError('Template Husky Experiment was not cloned to the storage.')
        results.done(True)

        #
        # Clone a Cloned Experiment
        #

        results.start('Cloning a cloned experiment')
        new_experiment_id = json.loads(vc.clone_cloned_experiment(experiment_id))['clonedExp']
        if new_experiment_id not in vc._VirtualCoach__get_experiment_list(cloned=True):
            raise TestCaseError('Cloning the cloned Template Husky Experiment failed')
        results.done(True)

        #
        # Delete a Cloned Experiment
        #

        results.start('Deleting a cloned experiment')
        vc.delete_cloned_experiment(new_experiment_id)
        if new_experiment_id in vc._VirtualCoach__get_experiment_list(cloned=True):
            raise TestCaseError('Deleting a cloned Experiment failed')
        results.done(True)

        #
        # Import an Experiment Folder
        #

        results.start('Importing an experiment folder')
        response = vc.import_experiment(path + '/test_experiment_folder')
        new_experiment_id = json.loads(response.text)['destFolderName']
        if new_experiment_id not in vc._VirtualCoach__get_experiment_list(cloned=True):
            raise TestCaseError('Importing an experiment folder failed')
        vc.delete_cloned_experiment(new_experiment_id)
        results.done(True)

        #
        # Import an Experiment Zipped Folder
        #

        results.start('Importing a zipped experiment folder')
        response = vc.import_experiment(path + '/test_experiment_folder.zip')
        new_experiment_id = json.loads(response.text)['destFolderName']
        if new_experiment_id not in vc._VirtualCoach__get_experiment_list(cloned=True):
            raise TestCaseError('Importing an experiment folder failed')
        vc.delete_cloned_experiment(new_experiment_id)
        results.done(True)

        #
        # Experiment Launch and Simulation State Interaction
        #

        # launch an experiment
        results.start('Launching Empty Template Husky Experiment')
        sim = vc.launch_experiment(experiment_id)
        results.done(True)

        # status handlers for simulation status events, simply write to our global status message
        # has to be a mutable type for nested inner function scope, so use a list of size one
        last_status = [None]

        def on_status(status):
            last_status[0] = status

        # helper to check if a condition is met for a status message, wait for either a condition
        # to be met (success) or a timeout to occur (failure)
        def wait_condition(timeout, description, condition):
            start = time.time()
            while time.time() < start + timeout:
                time.sleep(0.25)
                if condition(last_status[0]):
                    return
            raise TestCaseError(description)

        # register a ROS callback for simulation status
        results.start('Registering/Receiving Simulation Status')
        sim.register_status_callback(on_status)
        wait_condition(10, 'Waiting for any simulation status messages.', lambda x: x is not None)
        results.done(True)

        # start the simulation
        results.start('Starting Simulation')
        sim.start()
        wait_condition(5, 'Waiting for simulation to start.', lambda x: x['state'] == 'started')
        results.done(True)

        # wait until several seconds of simulation time have elapsed
        results.start('Ensuring Simulation Time Elapses')
        sim.start()
        wait_condition(30, 'Waiting for simulation to resume.', lambda x: x['simulationTime'] > 5.0)
        results.done(True)
        # pause the simulation
        results.start('Pausing Simulation')
        sim.pause()
        wait_condition(5, 'Waiting for simulation to pause.', lambda x: x['state'] == 'paused')
        results.done(True)

        # verify REST simulation state machines ROS simulation state
        results.start('Testing Simulation State REST Service')
        if sim.get_state() != 'paused':
            raise TestCaseError('REST Simulation State Does Not Match ROS State.')
        results.done(True)

        # resuming the simulation
        results.start('Resuming Simulation')
        sim.start()
        wait_condition(5, 'Waiting for simulation to resume.', lambda x: x['state'] == 'started')
        results.done(True)
        #
        # Transfer Function Interaction
        #

        # retrieve transfer functions
        results.start('Retrieving Transfer Functions')
        tfs = sim._Simulation__get_simulation_scripts('transfer-function')['data']
        if not tfs:
            raise TestCaseError('No transfer functions returned for experiment.')
        results.done(True)

        # create a valid tf with a modified import and an invalid tf for testing
        tf_name = str(list(tfs.keys())[0])
        valid_tf = 'import os\n' + tfs[tf_name]
        invalid_tf = 'this is invalid!\n' + tfs[tf_name]

        # valid update for a transfer function, should apply and keep sim running
        results.start('Valid Transfer Function Update')
        sim.edit_transfer_function(tf_name, valid_tf)
        tfs = sim._Simulation__get_simulation_scripts('transfer-function')['data']
        if tf_name not in tfs or tfs[tf_name] != valid_tf:
            raise TestCaseError('Transfer function update did not apply successfully.')
        wait_condition(5, 'Waiting for simulation to resume.', lambda x: x['state'] == 'started')
        if sim.get_state() != 'started':
            raise TestCaseError('Valid transfer function update did not resume simulation.')
        results.done(True)

        # invalid update should pause the simulation on failure
        results.start('Invalid Transfer Function Update')
        try:
            sim.edit_transfer_function(tf_name, invalid_tf)
        except Exception:
            pass
        tfs = sim._Simulation__get_simulation_scripts('transfer-function')['data']
        if tf_name not in tfs or tfs[tf_name] != valid_tf:
            raise TestCaseError('Transfer function did not revert after failure.')
        wait_condition(5, 'Waiting for simulation to pause.', lambda x: x['state'] == 'paused')
        if sim.get_state() != 'paused':
            raise TestCaseError('Failed transfer function update did not pause simulation.')
        results.done(True)

        sim.start()
        results.done(True)

        results.start('Adding a valid transfer function')
        valid_tf = """
@nrp.Robot2Neuron()
def tf(t):
    #log the first timestep (20ms), each couple of seconds
    if (t%2<0.02):
        clientLogger.info('Time: ', t)
"""
        sim.add_transfer_function(valid_tf)
        tfs = sim._Simulation__get_simulation_scripts('transfer-function')['data']
        if 'tf' not in tfs or tfs['tf'] != valid_tf:
            raise TestCaseError('Adding a new valid Transfer Function did not succeed.')
        if sim.get_state() != 'started':
            raise TestCaseError('Adding a new valid transfer function did not resume simulation.')

        sim.start()
        results.done(True)

        results.start("Adding an invalid transfer function")
        try:
            sim.add_transfer_function(invalid_tf)
        except Exception:
            tfs = sim._Simulation__get_simulation_scripts('transfer-function')['data']
            if 'invalid_tf' in tfs:
                raise TestCaseError('Adding a new invalid Transfer Function succeeded.')
            if sim.get_state() != 'paused':
                raise TestCaseError('Adding a new invalid Transfer Function did not pause'
                                    'simulation.')
        results.done(True)

        results.start("Deleting a transfer function")
        sim.delete_transfer_function('tf')
        tfs = sim._Simulation__get_simulation_scripts('transfer-function')['data']
        if 'tf' in tfs:
            raise TestCaseError('Deleting a Transfer Function did not succeed.')
        results.done(True)

        results.start("Deleting a nonexistent transfer function")
        try:
            sim.delete_transfer_function('nonexistent')
        except Exception:
            if tfs != sim._Simulation__get_simulation_scripts('transfer-function')['data']:
                raise TestCaseError('Deleting a nonexistent TF deleted something else.')
        results.done(True)
        #
        # State Machine Interaction
        #

        # add a new valid state machine
        results.start('Adding a new valid State Machine')
        sim.start()
        sms = sim._Simulation__get_simulation_scripts('state-machine')['data']
        valid_sm = 'import os\n'
        sim.add_state_machine('statemachine_valid', valid_sm)
        if len(sim._Simulation__get_simulation_scripts('state-machine')['data']) != len(sms) + 1:
            raise TestCaseError('Failed to add a new valid state machine')
        results.done(True)
        # valid state machine editing
        results.start('Valid State Machine Update')
        sim.start()
        valid_sm = 'import math\n' + sim.get_state_machine('statemachine_valid')
        sim.edit_state_machine('statemachine_valid', valid_sm)
        if sim.get_state_machine('statemachine_valid') != valid_sm:
            raise TestCaseError('State Machine did not update successfully.')
        wait_condition(5, 'Waiting for simulation to resume.', lambda x: x['state'] == 'started')
        if sim.get_state() != 'started':
            raise TestCaseError('Valid State Machine update did not resume running simulation.')
        results.done(True)

        #
        # Brain and Population Interaction
        #

        # modify brain script
        results.start('Modifying Brain Script')
        sim.start()
        brain_script = sim.get_brain()

        # modify brain in a valid way
        sim.edit_brain('import os\n' + brain_script)
        if 'import os' not in sim.get_brain():
            raise TestCaseError('Brain did not update successfully.')
        if sim.get_state() != 'started':
            raise TestCaseError('Brain update did not resume running simulation.')

        # modify brain in an invalid way
        try:
            sim.edit_brain('this is invalid\n' + brain_script)
        except Exception:
            pass
        if 'this is invalid' in sim.get_brain():
            raise TestCaseError('Invalid brain updated successfully.')
        if sim.get_state() != 'paused':
            raise TestCaseError('Invalid brain updated did not cause simulation to pause.')
        results.done(True)

        # modify populations
        results.start('Modifying Neural Populations')
        sim.start()

        populations = sim.get_populations()
        populations['record_test'] = populations['record']
        sim.edit_populations(populations)

        populations = sim.get_populations()
        if not populations['record_test']:
            raise TestCaseError('Population Step size update did not succeed.')
        if sim.get_state() != 'started':
            raise TestCaseError('Population step update did not resume running simulation.')

        results.done(True)

        #
        # Reset Different Simulation Parts
        #
        # TODO: reset full and world has been disabled, re-enable these tests accordingly after
        #  they are back
        # results.start('Resetting full simulation')
        # sim.reset('full')
        # wait_condition(5, 'Waiting for simulation to pause.', lambda x: x['state'] == 'paused')
        # sim.start()
        # results.done(True)

        # results.start('Resetting Environment')
        # sim.reset('world')
        # wait_condition(5, 'Waiting for simulation to pause.', lambda x: x['state'] == 'paused')
        # sim.start()
        # results.done(True)

        results.start('Resetting Robot Pose')
        sim.reset('robot_pose')
        wait_condition(5, 'Waiting for simulation to pause.', lambda x: x['state'] == 'paused')
        sim.start()
        results.done(True)

        #
        # Shutdown, Cleanup and Delete the cloned experiment used in the tests
        #

        # shutdown the experiment cleanly
        results.start('Stopping Simulation')
        sim.stop()
        wait_condition(5, 'Waiting for simulation to stop.', lambda x: x['state'] == 'stopped')
        vc.delete_cloned_experiment(experiment_id)
        results.done(True)

    # handle test case or unexpected failures, attempt to cleanup and terminate
    except Exception as e:

        # fail the last test case
        results.abort()

        # check if the test case has just failed
        print()
        if isinstance(e, TestCaseError):
            print('Test case failed: %s - attempting to cleanup.' % e)

        # unhandled exception means a virtual coach failure, log it
        else:
            print()
            print('Unexpected exception encountered during execution, attempting to cleanup.')
            print()
            traceback.print_exc(e)
        print()

        # if a sim is running, try to shut it down cleanly, we can't do much more
        if sim:
            try:
                sim.stop()
            except Exception:
                pass

    # make sure this file is properly updated for number of test cases, if not fail the dev
    if len(results) > NUM_TEST_CASES or (len(results) < NUM_TEST_CASES and results[-1].success):
        results.start('Developer: %i test case(s) vs %i expected!' % (len(results), NUM_TEST_CASES))
        results.done(False)

    # return the results for how far we made it
    return results


#
# Actual entry point, parse user options and print out results. Return a valid success (0) or
# failure (-1) exit code upon exit for any automated checking.
#
if __name__ == '__main__':

    # allow the user to run IT using an OIDC enabled local backend (None if omitted)
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose",
                        required=False,
                        action='store_true',
                        help="enable or disable all virtual coach logging")
    parser.add_argument("-u", "--oidc-username",
                        required=False,
                        type=str,
                        help="login required if OIDC is enabled on the local backend")
    parser.add_argument("-s", "--storage-username",
                        required=False,
                        help="login required to access files on local Storage Server")
    args = parser.parse_args()

    # if verbosity is disabled, turn off any logging from the virtual coach/other libraries
    if not args.verbose:
        logging.disable(logging.CRITICAL)

    # banner with user warnings / prompts
    print(''.center(80, '='))
    print('|%s|' % ''.center(78, ' '))
    print('|%s|' % 'VirtualCoach Integration Test Script'.center(78, ' '))
    print('|%s|' % ''.center(78, ' '))
    print('|%s|' % ''.center(78, ' '))
    if args.oidc_username:
        print('|%s|' % ('OIDC is enabled, user: %s' % args.oidc_username).center(78, ' '))
        print('|%s|' % 'Note: you may be prompted for a password.'.center(78, ' '))
    elif args.storage_username:
        print('|%s|' % ('Local Storage is enabled, user: %s' % args.storage_username)
              .center(78, ' '))
        print('|%s|' % 'Note: you may be prompted for a password.'.center(78, ' '))
    else:
        raise ValueError('|%s|' % 'No OIDC or local Storage credentials were provided. Please run '
                         'the script with either oidc_username or storage_username as arguments'.
                         center(78, ' '))
    print('|%s|' % ''.center(78, ' '))
    print('=%s=' % ''.center(78, '='))

    # run all of the sequential tests, results are returned upon success or first failure
    print()
    print('Running Test Cases, This May Take A Few Moments...'.center(80, ' '))
    print()
    cumulative = TestCase('Summary'.center(52, ' '))
    results = run(args.oidc_username, args.storage_username)
    cumulative.done(all([r.success for r in results]))
    results.append(cumulative)

    # print out a human readable table of results, texttable doesn't handle color well in Python 2.7
    # so we just do it manually here instead, not the most readable but functional
    widths = [75, 12, 12]
    headers = ['Description', 'Duration', 'Result']
    print('\n\n+{0}+{1}+{2}+'.format(*(''.center(w, '-') for w in widths)))
    print('|{0}|{1}|{2}|'.format(*(h.center(w, ' ') for h, w in zip(headers, widths))))
    print('+{0}+{1}+{2}+'.format(*(''.center(w, '=') for w in widths)))

    # print all of the data rows in easy to visually scan colors (since no one will look at the
    # individual test cases and output otherwise
    for r in results:

        # green or red for pass/fail using terminal color escape sequences
        color = '\x1b[92m' if r.success else '\x1b[91m'

        # format the columns with color strings and precision
        formatted = ['%s%s%s' % (color, r.description, '\x1b[0m'),
                     '%s%6.3f%s' % (color, r.time, '\x1b[0m'),
                     '%s%s%s' % (color, 'OK' if r.success else 'FAIL', '\x1b[0m')]

        # format the table output justification, offset included (needed with color strings and this
        # seems to be what texttable doesn't do properly)
        formatted = [formatted[0].ljust(widths[0] + 9, ' '),
                     formatted[1].center(widths[1] + 9, ' '),
                     formatted[2].center(widths[2] + 9, ' ')]

        # print the row and next table separator
        print('|{0}|{1}|{2}|'.format(*formatted))
        print('+{0}+{1}+{2}+'.format(*(''.center(w, '-') for w in widths)))

    # change the exit code so it can be checked automatically if needed
    sys.exit(0 if results[-1].success else -1)
