1. Description

  This example demonstrates Transfer Function optimization through parameter space searching. It
  takes the baseline Braitenberg Mouse in Y-Maze experiment and attempts to:

  - reduce overall jitter when the mouse is focusing on the target
  - determine reasonable RGB values to detect a new color

  This is achieved by running many experiment trials with automated simulation state and stimulus
  input, analyzing and visualizing the resulting data, and finally running an interactive trial
  where the user can evaluate the results.

  All data necessary to run the Jupyter Notebook without running the individual parameter trials
  is included. You can choose to uncomment lines in the notebook to delete the provided data and
  run your own trails, though this may take some time.


2. Install Dependencies

  2.1 Install a local Jupyter Notebook:

    http://jupyter.readthedocs.io/en/latest/install.html

  2.2 Install any needed requirements for this example:

    source $HBP/VirtualCoach/platform_venv/bin/activate
    pip install -r $HBP/VirtualCoach/examples/tf_parameter_search/requirements.txt
    deactivate


3. Running This Example in a Jupyter Notebook

  3.1 Start your local NRP installation:

    refer to latest NRP documentation

  3.2 Launch the Jupter Notebook in the VirtualCoach environment:

    cle-virtual-coach jupyter notebook

  3.3 Load the supplied notebook $HBP/VirtualCoach/examples/tf_parameter_search/tf_parameter_search.ipynb
